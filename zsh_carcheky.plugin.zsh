#!/bin/bash

######################################################
######### ZSH CARCHEKY:
######################################################

### dkhelp:                 print this help
alias dkhelp="cat ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh_carcheky/*sh | grep \#\#\#"

### dkupdate:               update plugins zsh_carcheky
function dkupdate() {
  currentdir=$(pwd)
  cd ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh_carcheky
  git reset --hard
  git fetch --prune
  git pull -f
  source ~/.zshrc
  cd ${currentdir}
}

### composer-install:       install composer
function composer-install() {
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  php composer-setup.php
  php -r "unlink('composer-setup.php');"
  sudo mv composer.phar /usr/local/bin/composer
  echo "==> composer instalado en /usr/local/bin/composer"
}

### add_alias:              add alias to ~/MISALIAS
function add_alias() {
  echo "nombre:"
  read nombre
  echo "comando:"
  read comando
  [[ ! -f ~/MISALIAS ]] && touch ~/MISALIAS && echo "source ~/MISALIAS" >>~/.bashrc && [[ -f ~/.zshrc ]] && echo "source ~/MISALIAS" >>~/.zshrc
  echo "alias ${nombre}='${comando}'" >>~/MISALIAS
  echo "alias instalado"
  cat ~/MISALIAS | grep ${nombre}
  zsh
}

function script_install_zsh() {
  wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/install-zsh.sh | bash
}

function script_install_ddev() {
  wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/install-ddev.sh | bash
}

function script_install_docker() {
  wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/install-docker.sh | bash
}

## load aliases,
# Si la variable ZSH_CARCHEKY_DOCKER no está definida o es igual a 1, carga el archivo zsh_carcheky.docker.sh
if [[ -z "${ZSH_CARCHEKY_DOCKER:-}" || "${ZSH_CARCHEKY_DOCKER:-}" == "1" ]]; then
  source "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh_carcheky/zsh_carcheky.docker.sh"
fi

######################################################
