#!/bin/bash

######################################################
######### DOCKER:
#########   disable with ZSH_CARCHEKY_DOCKER=0
######################################################

### dkr:                    docker compose exec container "$@"
function dkr() {
  # docker-compose exec php "$@"
  # if exists a runing container with name_php
  # use it to exec docker-compose exec php
  # else use the name_webapp container
  if [[ $(docker ps --filter "name=.*_php" --format '{{.Names}}') ]]; then
    docker-compose exec php "$@"
  else
    docker-compose exec webapp "$@"
  fi
}

### dks:                    docker compose stop
function dks() {
  docker stop $(docker ps -a -q) &>/dev/null
}

### dkk:                    docker compose kill
function dkk() {
  docker kill $(docker ps -a -q) &>/dev/null
}

### dku:                    docker compose up
alias dku="docker compose up"

### dkud:                   docker compose up -d
alias dkud="docker compose up -d"

### dkudp
alias dkudp="docker compose up -d --pull always"

### dkudb
alias dkudb="docker compose up -d --build"

### dkudpb
alias dkudpb="docker compose up -d --pull always --build"

### dkdown:                 remove containers
alias dkdown="docker compose down"

## aliasoverride:     override alias
# alias aliasoverride="source ~/.oh-my-zsh/custom/plugins/zsh_carcheky/zsh_carcheky.aliases.sh"

# ## aliasinstall:     override alias
# alias aliasinstall="echo '' >> ~/.zshrc ; echo 'source ~/.oh-my-zsh/custom/plugins/zsh_carcheky/zsh_carcheky.aliases.sh' >> ~/.zshrc ; zsh"

## dklog:      docker compose logs -f
alias dklog="docker compose logs -f"

# ## dkb:      docker compose exec $(checkenv) bash
# alias dkb="docker compose exec $(checkenv) bash"

# ## dkz:      docker compose exec $(checkenv) zsh
# alias dkz="docker compose exec $(checkenv) zsh"

# ## dkprune:    docker volume prune -f; docker image prune -a -f;
# alias dkprune="docker volume prune -f; docker image prune -a -f;"

# ## dkpruneall:    dks ;  docker system prune -a -f; docker container prune -f ; docker volume prune -f; docker image prune -a -f; docker builder prune -a -f;
# alias dkpruneall="dks ;  docker system prune -a -f; docker container prune -f ; docker volume prune -f; docker image prune -a -f; docker builder prune -a -f;"

# ## dkrebuild:  dks; docker compose down; dkprune; dkudb
# alias dkrebuild="dkub; docker compose restart"

# ## dkdown:     remove containers
# alias dkdown="docker compose down"

# ## zshalpine: copy zsh alpine script
# alias zshalpine="echo 'curl -s https://gitlab.com/-/snippets/2194683/raw/main/install-zsh-alpine-wodby.sh | bash ; zsh ; exit'| xclip ; dkr bash"

# ## zshalpinew: (windows) copy zsh alpine script
# alias zshalpinew="echo 'curl -s https://gitlab.com/-/snippets/2194683/raw/main/install-zsh-alpine-wodby.sh | bash ; zsh ; exit'| clip.exe ; dkr bash"

# ## zshdebian: copy zsh alpine script
# alias zshdebian="echo 'curl -s https://gitlab.com/-/snippets/2194683/raw/main/install-zsh-debian-root.sh | bash ; zsh ; exit'| xclip ; dkr bash"

# ## zshdebianw: (windows) copy zsh alpine script
# alias zshdebianw="echo 'curl -s https://gitlab.com/-/snippets/2194683/raw/main/install-zsh-debian-root.sh | bash ; zsh ; exit'| clip.exe ; dkr bash"

# ## dklaunch:   powershell.exe \"start '$docker_destop'\"
# alias dklaunch="powershell.exe \"start '$docker_destop'\""

## wslshutdown:    powershell.exe \"wsl --shutdown\""
alias wslshutdown="powershell.exe \"wsl --shutdown\""

# ## dkdrush:   dkr drush
# alias dkdrush="dkr drush"

# ## dkcomposer:   dkr composer
# alias dkcomposer="dkr composer"
######################################################

function sass-compile(){
  if [ $1 ]; then
    docker run --rm --pull always -v $(pwd)/scss:/app/scss -v $(pwd)/css:/app/css carcheky/sass-compiler "$@"
  else
    docker run --rm --pull always -v $(pwd)/scss:/app/scss -v $(pwd)/css:/app/css carcheky/sass-compiler
  fi
}

alias drush="ddev drush"
alias composer="ddev composer"
alias grumphp="ddev grumphp"