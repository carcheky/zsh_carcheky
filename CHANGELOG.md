## [2.6.1](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.6.0...v2.6.1) (2025-01-14)


### Bug Fixes

* dkudb & dkudpb ([a225be7](https://gitlab.com/carcheky/zsh_carcheky/commit/a225be78fde7d1530d06f762f5f4939ad98361df))

# [2.6.0](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.5.1...v2.6.0) (2025-01-14)


### Bug Fixes

* scripts ([5d6d9d0](https://gitlab.com/carcheky/zsh_carcheky/commit/5d6d9d09bf2d1c82b15bbe50be1d8068d7dea104))


### Features

* add dkudb and dkudpb ([6d91627](https://gitlab.com/carcheky/zsh_carcheky/commit/6d91627eaef9ce8c9e0c167bb265a1208603500b))

## [2.5.1](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.5.0...v2.5.1) (2024-04-24)


### Bug Fixes

* no output ([acf7efc](https://gitlab.com/carcheky/zsh_carcheky/commit/acf7efc83819be5bbf96ba4f9201a6490bf4497e))

# [2.5.0](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.4.0...v2.5.0) (2024-04-24)


### Features

* dkk ([964547d](https://gitlab.com/carcheky/zsh_carcheky/commit/964547de7a98c9558fb3f12e04c5fe9e40530ded))

# [2.4.0](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.3.0...v2.4.0) (2024-03-13)


### Features

* dkudp ([3228506](https://gitlab.com/carcheky/zsh_carcheky/commit/322850620a162f519bed880c2c215f5281569e89))

# [2.3.0](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.2.1...v2.3.0) (2024-03-10)


### Features

* added ddev aliases ([5c7280a](https://gitlab.com/carcheky/zsh_carcheky/commit/5c7280ad8475408b1e15f5c87db859cd26b9a467))
* install scripts ([83d333b](https://gitlab.com/carcheky/zsh_carcheky/commit/83d333be5fc5c0bc15dd26b245a61432ac66c88c))

## [2.2.1](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.2.0...v2.2.1) (2023-05-12)


### Bug Fixes

* composer install ([6d10d9a](https://gitlab.com/carcheky/zsh_carcheky/commit/6d10d9a7025f6b1c9a82abe0dcdf9191f569d30b))

# [2.2.0](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.1.2...v2.2.0) (2023-05-11)


### Features

* added 'dkdown' alias ([e637fde](https://gitlab.com/carcheky/zsh_carcheky/commit/e637fdef6dd1262064607e2294b859232d80e096))

## [2.1.2](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.1.1...v2.1.2) (2023-05-10)


### Bug Fixes

* docker functions not load due bad conditional ([06bcb90](https://gitlab.com/carcheky/zsh_carcheky/commit/06bcb90607c7d7411717e547041b6ff168d593a6))

## [2.1.1](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.1.0...v2.1.1) (2023-05-10)


### Bug Fixes

* move dkhelp to correct section ([aed740f](https://gitlab.com/carcheky/zsh_carcheky/commit/aed740fed1fa954d7a55b75c5f6833d803d90ef4))
* remove output from some commands ([a1883f9](https://gitlab.com/carcheky/zsh_carcheky/commit/a1883f9c8aa19ad9ee5ef67a7550eacc9c517e26))

# [2.1.0](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.0.1...v2.1.0) (2023-05-10)


### Features

* allow disable docker functions ([bf1fefe](https://gitlab.com/carcheky/zsh_carcheky/commit/bf1fefe4f99251e09b5142e1b3c52f317d2e8588))

## [2.0.1](https://gitlab.com/carcheky/zsh_carcheky/compare/v2.0.0...v2.0.1) (2023-05-10)


### Bug Fixes

* remove info output from dkr ([5d2d5ef](https://gitlab.com/carcheky/zsh_carcheky/commit/5d2d5ef7e74b09fbe8134571d9a5d9e749681fca))

# [2.0.0](https://gitlab.com/carcheky/zsh_carcheky/compare/v1.1.0...v2.0.0) (2023-05-09)


### Features

* only basics ([4d23d61](https://gitlab.com/carcheky/zsh_carcheky/commit/4d23d6122cbd6204e00bfc4fb8e6403a9b88e68e))


### BREAKING CHANGES

* full rewrited and cleaned

# [1.1.0](https://gitlab.com/carcheky/zsh_carcheky/compare/v1.0.2...v1.1.0) (2023-05-09)


### Features

* dklog ([baa165f](https://gitlab.com/carcheky/zsh_carcheky/commit/baa165fbe6e0d43748e822bc1e49e15680066f26))
* new aliases ([5f4a5cf](https://gitlab.com/carcheky/zsh_carcheky/commit/5f4a5cf8ebb4eec1263fce13a531b3df35969408))
